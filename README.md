# urt-server

Urban Terror Server

## Usage

```bash
# run server
docker run --name urt \
	       -p 27960:27960/udp \
	       registry.gitlab.com/sebastianhutter/urt-server
```

## Persistence
The image exposes a volume with all configuration files for the system
```bash
# run with persistent volume
docker run --name urt \
		   -p 27960:27960/udp \
		   -v urt-config:/config \
		   registry.gitlab.com/sebastianhutter/urt-server

# run with a host volume 
# if you run with a host volume you need to copy the server.cfg, autoexec.cfg and mapcycle.txt from another urbanterror installation 
# or from the base image
docker run --name urt \
		   -p 27960:27960/udp \
		   -v $PWD/urt-config:/config \
		   registry.gitlab.com/sebastianhutter/urt-server
```
