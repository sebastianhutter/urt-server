#!/bin/bash

# first make sure the urt user has permission on the config directory
chown -R urt:urt ${URTCONFIG}

# run urt server
# still keep shell as parent to properly 
# listen to interrupts
su - urt /bin/bash -c "\
	${URTPATH}/Quake3-UrT-Ded.x86_64 \
		+set fs_game q3ut4 \
		+set fs_basepath ${URTPATH} \
		+set fs_homepath ${URTPATH} \
		+set dedicated 2 \
		+set net_port 27960 \
		+set com_hunkmegs 128 \
		+exec server.cfg \
"

