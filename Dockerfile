FROM debian:stable-slim

# current urban terror version
ENV URTVERSION 43
ENV URTPATH /urbanterror
ENV URTCONFIG /config

# install curl for download of UT
RUN apt-get update \
  && apt-get install -y curl libxml2-utils \
  && rm -rf /var/lib/apt/lists/*

# set the workdirectory
RUN mkdir -p ${URTPATH} 
WORKDIR ${URTPATH}

# the download of urban terror is a little more complicated
# we need to generate a valid download link first
# to generate the download link we need to know the hosts public ip
# and send it as post to the download link and then get the download url from the curl output
RUN export ip=$(curl -s ifconfig.co) \
  && export download_url=$(curl -s -X POST -F "ip=$ip" \
  		https://www.urbanterror.info/downloads/software/urt/${URTVERSION}/UrbanTerror${URTVERSION}_ded.tar.gz?download=direct \
  		| grep -Po '"https.*key=.*?"' | cut -f2 -d'"') \
  && curl -L -s ${download_url} -o ut.tar.gz \
  && tar --strip-components 1 -xzf ut.tar.gz \
  # echo is required to accept the license
  && echo "y" | ASKBEFOREUPDATING=0 bash UrTUpdater_Ded.sh

# que create config files
RUN mkdir -p ${URTCONFIG} \
  && cp ${URTPATH}/q3ut4/server_example.cfg ${URTCONFIG}/server.cfg \
  && ln -s ${URTCONFIG}/server.cfg ${URTPATH}/q3ut4/server.cfg \
  && cp ${URTPATH}/q3ut4/autoexec_example.cfg ${URTCONFIG}/autoexec.cfg \
  && ln -s ${URTCONFIG}/autoexec.cfg ${URTPATH}/q3ut4/autoexec.cfg \
  && cp ${URTPATH}/q3ut4/mapcycle_example.txt ${URTCONFIG}/mapcycle.txt \
  && ln -s ${URTCONFIG}/mapcycle.txt ${URTPATH}/q3ut4/mapcycle.txt
VOLUME ${URTCONFIG}

# games.log is pushed to /dev/null 
# we already have all the required log in stdout
# https://serverfault.com/questions/599103/make-a-docker-application-write-to-stdout
RUN ln -sf /dev/null ${URTPATH}/q3ut4/games.log

# add urt user
RUN useradd -d ${URTPATH} urt \
  && chown -R urt:urt ${URTPATH}

# add entrypoint 
ADD docker-entrypoint.sh /
RUN chmod +x /docker-entrypoint.sh ${URTPATH}/Quake3-UrT-Ded.x86_64

# expose default tcp port for game server
EXPOSE 27960/udp

ENTRYPOINT [ "/docker-entrypoint.sh" ]
